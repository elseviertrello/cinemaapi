const express = require("express");
const router = express.Router();
const OpeningTimes = require('../schemas/openingtimes.model.js');
const bodyParser = require("body-parser");

router.use(bodyParser.json())

router.get('/', function (req, res) {
    OpeningTimes.find()
        .then(times => {
            if (!OpeningTimes) {
                return res.status(404).send({ message: "Error: Opening times not found!" });
            }
            res.send(times);
        }).catch(err => {
            if (err.kind === "ObjectID") {
                return res.status(404).send({
                    message: "Error: Film data not found."
                });
            }
            return res.status(500).send({
                message: "Error: Film data not found."
            });
        });
});

router.post('/', (req, res) => {
    let openingData = new OpeningTimes({
        day: req.body.day,
        opening: req.body.opening,
        close: req.body.close
    });
    openingData
        .save(openingData)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "An error occurred while adding the times."
            });
        });
});

router.get("/:id", function (req, res) {
    OpeningTimes.findById(req.params.id)
        .then(time => {
            if (!time) {
                return res.status(404).send({
                    message: "Game not found with id" + req.params.id
                });
            }
            res.send(time);
        });
});

router.delete("/:id", function (req, res) {
    OpeningTimes.findByIdAndRemove(req.params.id)
        .then(time => {
            if (!time) {
                return res.status(404).send({
                    message: "Data not removed" + req.params.id
                });
            }
            res.send(time);
        });
});

module.exports = router;