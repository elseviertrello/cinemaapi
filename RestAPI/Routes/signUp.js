const express = require("express");
const router = express.Router();
const signUp = require("../schemas/signUp.model.js");
const bodyParser = require("body-parser");

router.use(bodyParser.json())

router.post('/', (req, res) => {
    let newUser = new signUp({
        userTitle: req.body.userTitle,
        fname: req.body.fname,
        lname: req.body.lname,
        email: req.body.email,
        number: req.body.number,
        dob: req.body.dob,
        gender: req.body.gender
    });
    newUser
        .save(newUser)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "An error occurred while adding the User."
            });
        });
});

router.delete("/:id", (req, res) => { /*CHANGE URL WHEN MERGING WITH OPENINGS FILE*/
    signUp.findByIdAndRemove(req.params.id)
        .then(user => {
            if (!user) {
                return res.status(404).send({ message: "User with ID " + req.params.id + " not found." });
            }
            res.send(user);
        }).catch(err => {
            if (err.kind === "ObjectID") {
                return res.status(404).send({
                    message: "User with ID " + req.params.id + " not found."
                })
            }
            return res.status(500).send({
                message: "User with ID " + req.params.id + " not found."
            });
        });
});

//router.delete("/", (req, res) => { /*CHANGE URL WHEN MERGING WITH OPENINGS FILE*/
//  signUp.findOneAndDelete()
//    .then(user => {
//      if (!user) {
//        return res.status(404).send({ message: "User with ID " + req.params.id + " not found." });
//  }
//res.send(user);
//  }).catch(err => {
//    if (err.kind === "ObjectID") {
//      return res.status(404).send({
//        message: "User with ID " + req.params.id + " not found."
//  })
// }
// return res.status(500).send({
//   message: "User with ID " + req.params.id + " not found."
// });
// });
//});

router.get('/', (req, res) => {
    signUp.find()
        .then(user => {
            if (!signUp) {
                return res.status(404).send({ message: "Error: User not found." });
            }
            res.send(user);
        }).catch(err => {
            if (err.kind === "ObjectID") {
                return res.status(404).send({
                    message: "Error: User not found."
                })
            }
            return res.status(500).send({
                message: "Error: User not found."
            });
        });
});

module.exports = router;