const express = require("express");
const router = express.Router();
const allFilms = require("../schemas/showings.model.js");
const bodyParser = require("body-parser");

router.use(bodyParser.json())


router.get('/', (req, res) => {
    allFilms.find()
        .then(films => {
            if (!allFilms) {
                return res.status(404).send({ message: "Error: Film data not found." });
            }
            res.send(films);
        }).catch(err => {
            if (err.kind === "ObjectID") {
                return res.status(404).send({
                    message: "Error: Film data not found."
                })
            }
            return res.status(500).send({
                message: "Error: Film data not found."
            });
        });
});

router.post('/', (req, res) => {
    let newFilm = new allFilms({
        title: req.body.title,
        synopsis: req.body.synopsis,
        cast: req.body.cast,
        directors: req.body.directors,
        showingTimes: req.body.showingTimes,
        releaseDate: req.body.releaseDate,
        filmStatus: req.body.filmStatus,
        img: req.body.img
    });
    newFilm
        .save(newFilm)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "An error occurred while adding the Film."
            });
        });
});

router.delete("/:id", (req, res) => { /*CHANGE URL WHEN MERGING WITH OPENINGS FILE*/
    allFilms.findByIdAndRemove(req.params.id)
        .then(film => {
            if (!film) {
                return res.status(404).send({ message: "Film with ID " + req.params.id + " not found." });
            }
            res.send(film);
        }).catch(err => {
            if (err.kind === "ObjectID") {
                return res.status(404).send({
                    message: "Film with ID " + req.params.id + " not found."
                })
            }
            return res.status(500).send({
                message: "Film with ID " + req.params.id + " not found."
            });
        });
});
//Edit the code below to find films by ID eventually

/*app.get("/guitars/:id", (req, res) => {
    Guitars.findById(req.params.id)
        .then(guitar => {
            if (!guitar) {
                return res.status(404).send({ message: "Guitar with ID " + req.params.id + " not found." });
            }
            res.send(guitar);
        }).catch(err => {
            if (err.kind === "ObjectID") {
                return res.status(404).send({
                    message: "Guitar with ID " + req.params.id + " not found."
                })
            }
            return res.status(500).send({
                message: "Guitar with ID " + req.params.id + " not found."
            });
        });
});*/

module.exports = router;