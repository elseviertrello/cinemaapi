const mongoose = require("mongoose");

const signUp = mongoose.Schema({
    userTitle: String,
    fname: String,
    lname: String,
    email: String,
    number: Number,
    dob: Date,
    gender: String
})

module.exports = mongoose.model("signUp", signUp);