import React from 'react';

const FormComponentText = props => {
    return (
        <div className={props.className}>
            <label htmlFor={props.id} className="col-sm-2 col-form-label">{props.label}</label>
            <div className="col-sm-4">
                <input type={props.type}
                    id={props.id}
                    placeholder={props.placeholder}
                    className="form-control"
                    required={props.required}
                //onChange = {handle   [insert some identifier]    Change}
                />
            </div>
        </div>
    );
};

export default FormComponentText;