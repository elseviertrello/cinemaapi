import React, { useState } from 'react';
import { HANDS } from '../../js/constants/hands.js';
import FormComponentText from './FormComponentText';
import FormComponentDropDown from './FormComponentDropDown';
import FormComponentRadio from './FormComponentRadio';
import axios from "axios";
//import GenerateUserId from '../../utils/generateId';

const USEURL = 'http://elsevier8.conygre.com:8080/signUp'

const SignUp = props => {

    const postUser = async user => {
        try {
            console.log("In post");
            await axios.post(USEURL, user)
            .then (res => {
                console.log(res.data); this.props.push("/index");
            
            });
            console.log("In post");
        } catch (e) {
            console.log(e);
        }
    }

    const testSubmit = () => {
        
        //event.preventDefault();
        var newUser = {
            fname: document.getElementById("fname").value,
            lname: document.getElementById("lname").value,
            userTitle: document.getElementById("title").value,
            email: document.getElementById("email").value,
            dob: document.getElementById("signupFormDOB").value,
            number: document.getElementById("number").value,
            gender: document.getElementById("gender").value
        };

        postUser(newUser);

        console.log(newUser);

    }

    return (
        <div className="container">
            <div className="container">
                <img className='w-50' src={HANDS.src} alt={HANDS.alt} />
            </div>

            <form onSubmit={testSubmit} className="form centered container">
                <div><FormComponentDropDown label="Title *" placeholder="Select Title..." id="title" required />

                </div>
                <FormComponentText
                    className="form-group row"
                    label="First Name *"
                    type="text" id="fname"
                    placeholder="First Name"
                    required />

                <FormComponentText
                    className="form-group row"
                    label="Last Name *"
                    type="text"
                    id="lname"
                    placeholder="Last Name"
                    required />

                <FormComponentText
                    className="form-group row"
                    label="Email Address *"
                    type="email"
                    id="email"
                    placeholder="Email Address"
                    required />

                <FormComponentText
                    className="form-group row"
                    label="Date of Birth *"
                    type="date"
                    id="signupFormDOB"
                    placeholder="Date of Birth"
                    required />

                <FormComponentText
                    className="form-group row"
                    label="Telephone Number"
                    type="text" pattern="[0-9]*"
                    id="number"
                    placeholder="Telephone Number"
                />
                <div>
                    <FormComponentRadio name="signupFormGender"
                        id="gender"
                        value="Male"
                        label="Male"
                    />
                    <FormComponentRadio name="signupFormGender"
                        id="gender"
                        value="Female"
                        label="Female"
                    />
                </div>

                <div className="form-group btn btn-submit">
                    <input type="submit"
                        name="submitButton"
                        id="submit"
                        value="Sign me up!">
                    </input>
                </div>
            </form >
        </div >

    );
}

export default SignUp;